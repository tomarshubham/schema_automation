import re
import pandas as pd


# Function to find the type of the object of Column.
def find_type(f):
    check_type = str(f)
    value_for_type = 0
    if re.search(r'^[\d]{0,20}$', check_type) or re.search(r'^[\-][\d]{0,20}$', check_type):
        return 'BIGINT', value_for_type

    if re.search(r'^[\d]{0,20}\.[\d]{0,20}$', check_type) or re.search(r'^\.[\d]{0,20}$', check_type) or re.search(
            r'^[\-][\d]{0,20}\.[\d]{0,20}$', check_type) or re.search(r'^[\-]\.[\d]{0,20}$', check_type):
        return 'Float', value_for_type + 1

    return 'String(200)', value_for_type + 2


# Function to find the primary key.
def find_primary_key(df):
    df_shape = df.shape[0]
    for i in df.columns:
        if len(list(df[i].unique())) == df_shape:
            return i


# Function to reformat the column names.
def reformat_col_headers(df):
    col_list = list(df.columns.values)
    processed_col_list = [re.sub(r'\s+|[/@$#&()!]', '_', str(item).strip().lower()) for item in
                          col_list]  # Reformat Column Headers
    processed_col_list = [x.replace("-", "_").replace(")", "").replace("(", "").replace("__", "_")
                          for x in processed_col_list]
    column_dictionary = dict(zip(col_list, processed_col_list))
    df.rename(columns=column_dictionary, inplace=True)
    return df


# Main function where all processing and functioning is done.
def processing_data(file, date_columns, *cred):
    global path_list
    path_list = list(name_csv(file).split('\\'))
    data = pd.read_csv(file)
    data = reformat_col_headers(data)
    reformat_dates = []
    if date_columns is not None:
        dates = date_columns.split(',')
        reformat_dates = [re.sub(r'\s+|[/@$#&()!]', '_', str(item).strip().lower()) for item in dates]
        reformat_dates = [x.replace("-", "_").replace(")", "").replace("(", "").replace("__", "_")
                          for x in reformat_dates]
        for i in reformat_dates:
            if i in list(data.columns):
                data.drop(i, axis=1, inplace=True)
    global user_credential
    user_credential = []
    for i in cred[0]:
        user_credential.append(i)

    primary_key = find_primary_key(data)
    types_final = []
    for i in list(data.columns):
        temp = 'BIGINT'
        type_val = 0
        for j in range(100):
            type_name, val = find_type(data.iloc[j][i])
            if type_name != temp and val > type_val:
                temp = type_name
        types_final = types_final + [temp]

    dictionary_for_column_type = {}
    if primary_key in list(data.columns):
        for i in range(len(list(data.columns))):
            if list(data.columns)[i] == primary_key:
                dictionary_for_column_type[list(data.columns)[i]] = "Column({}, primary_key=True)".\
                    format(types_final[i])
            else:

                if date_columns is not None:
                    for a in reformat_dates:
                        dictionary_for_column_type[a] = "Column(DateTime, nullable=True)"
                dictionary_for_column_type[list(data.columns)[i]] = "Column({}, nullable=True)".format(types_final[i])
    else:
        for i in range(len(list(data.columns))):
            dictionary_for_column_type['id'] = "Column(BIGINT, primary_key=True, autoincrement=True)"

            if date_columns is not None:
                for b in reformat_dates:
                    dictionary_for_column_type[b] = "Column(DateTime, nullable=True)"
            dictionary_for_column_type[list(data.columns)[i]] = "Column({}, nullable=True)".format(types_final[i])

    dictionary_for_row_type = {}
    for i in range(len(list(data.columns))):
        dictionary_for_row_type[list(data.columns)[i]] = "row['{}']".format(list(data.columns)[i])
        if date_columns is not None:
            for c in reformat_dates:
                dictionary_for_row_type[c] = "row['{}']".format(c)

    global column_automation
    global row_automation
    column_automation = ''.join('\t{} = {}\n'.format(k, v) for k, v in dictionary_for_column_type.items())
    row_automation = ''.join('\n\t\t\t{}={}'.format(k, v) for k, v in dictionary_for_row_type.items())
    row_automation = row_automation.replace(']', '],')
    row_automation = row_automation[:-1]
    schema_object = schema_func(file_in_csv, date_columns)
    print(schema_object)


# Function for schema automation.
def schema_func(file_in_csv, date_columns):
    schema = "import pandas as pd" \
             "\nimport re" \
             "\nimport sqlalchemy.sql" \
             "\nfrom sqlalchemy import create_engine" \
             "\nfrom sqlalchemy.orm import sessionmaker" \
             "\nfrom sqlalchemy.ext.declarative import declarative_base" \
             "\nfrom sqlalchemy import Column, String, DateTime, Float, BIGINT" \
             "\n\n" \
             "\ndef reformat_col_headers(df):" \
             "\n\tcol_list = list(df.columns.values)" \
             "\n\tprocessed_col_list = [re.sub(r'\s+|[/@$#&()!]', '_', " \
             "str(item).strip().lower()) for item in col_list]" \
             "\n\tprocessed_col_list = [x.replace(r'-', '_')" \
             ".replace(')', '').replace('(', '').\n\t\treplace('__', '_') for x in processed_col_list]" \
             "\n\tcoldict = dict(zip(col_list, processed_col_list))" \
             "\n\tdf.rename(columns=coldict, inplace=True)" \
             "\n\treturn df" \
             "\n" \
             "\n\nbase = declarative_base()\n\n" \
             "\nclass TableClass(base):" \
             "\n\t__tablename__ = 'table_{}'".format(path_list[-1]) + \
             "\n\t__table_args__ = {'extend_existing': True}" \
             + "\n{}".format(column_automation) + \
             "\n" \
             "\ndef table_format(filename, date_columns):" \
             "\n\tdb_string = \"postgres+psycopg2://{}:{}@{}/{}\"" \
             + ".format('{}', '{}', '{}', '{}')".format(user_credential[0], user_credential[1], user_credential[2],
                                                        user_credential[3]) + \
             "\n\tengine = create_engine(db_string)" \
             "\n\tbase.metadata.create_all(engine)" \
             "\n\tprint('Schema_Created...')" \
             "\n\tdf = pd.read_csv(filename)" \
             "\n\tdf = reformat_col_headers(df)" \
             "\n\tdates = date_columns.split(',')" \
             "\n\treformat_dates = [re.sub(r'\s+|[/@$#&()!]', '_', str(item).strip().lower()) for item in dates]" \
             "\n\treformat_dates = [x.replace('-', '_').replace(')', '').replace('(', '').replace('__', '_') " \
             "for x in reformat_dates]" \
             "\n\tfor i in reformat_dates:" \
             "\n\t\tif i in list(df.columns):" \
             "\n\t\t\tdf[i] = pd.to_datetime(df[i], dayfirst=True)" \
             "\n\tdf.fillna(sqlalchemy.sql.null(), inplace=True)" \
             "\n\tsession1 = sessionmaker(engine)" \
             "\n\tsession = session1()" \
             "\n\tc = 0" \
             "\n\tfor idx, row in df.iterrows():" \
             + "\n\t\tnew_entry = TableClass({})".format(row_automation) + \
             "\n\t\tsession.add(new_entry)" \
             "\n\t\tsession.commit()" \
             "\n\t\tprint('Written for Row {} of {}'.format(c, len(df)))" \
             "\n\t\tc += 1" \
             "\n\tsession.close()" \
             "\n\tprint('values_added')" \
             "\n\n" \
             "\nif __name__ == '__main__':" \
             + "\n\tdate_columns = '{}'".format(date_columns) + \
             "\n\tfile = r'{}'".format(file_in_csv) + \
             "\n\ttable_format(file, date_columns)"

    return schema


# Function to get the path details without .csv extension.
def name_csv(filename):
    file_without_csv = filename.split('/')
    return file_without_csv[-1].replace('.csv', '')


if __name__ == '__main__':

    #   Taking the csv file(must be in csv utf-8) and user's database credentials.
    file_in_csv = r'C:\Users\shubh\OneDrive\Desktop\project\v_stock.csv'
    user_cred = 'postgres', 'shubham', 'localhost', 'postgres'

    #  If file not contain Date column then type - None and Date name must not contain comma.
    date_columns = 'ENTDT'
    processing_data(file_in_csv, date_columns, user_cred)

    # File handling- making a new python file in same location where csv is stored.
    auto_schema = schema_func(file_in_csv, date_columns)
    file_name = open(r"{}_schema.py".format(name_csv(file_in_csv)), 'w')
    file_name.write(auto_schema)
    file_name.close()
