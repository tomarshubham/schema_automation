import unittest
from schema_automation import find_type, find_primary_key, reformat_col_headers, name_csv
import pandas as pd

def setUpModule():
    print('Unit testing starting for Schema Automation - ')

def tearDownModule():
    print('Unit testing done for the Schema Automation.')


class Test(unittest.TestCase):
    def setUp(self):
        print('Testing for the Schema Automation Methods')

    # to check the type (integer, Float or string).
    def test_find_type(self):

        self.assertEqual(find_type(1234), ('BIGINT', 0))
        self.assertEqual(find_type('1234'), ('BIGINT', 0))
        self.assertEqual(find_type('1223,we'), ('String(200)', 2))
        self.assertEqual(find_type(1234.121), ('Float', 1))
        self.assertEqual(find_type(.121), ('Float', 1))
        self.assertEqual(find_type(0.0), ('Float', 1))
        self.assertEqual(find_type("this is a string"), ('String(200)', 2))
        self.assertEqual(find_type('12/02/2010'), ('String(200)', 2))

    # to check the primary key(unique column with no null value) if exists.
    def test_primary_key(self):

        df_primary_key = pd.DataFrame({'primary key': [1, 2, 3, 4], 'not primary': [1, 1, 12, 2]},
                                      index=[1, 2, 3, 4])
        df_not_primary = pd.DataFrame({'not primary key': [1, 1, 3, 4], 'not primary': [1, 1, 12, 2]},
                                      index=[1, 2, 3, 4])
        df_primary_key_sec = pd.DataFrame({'not primary': [2, 2, 3, 4], 'primary key': [1, 2, 3, 4]},
                                          index=[1, 2, 3, 4])
        df_both_primary = pd.DataFrame({'primary key 1': [1, 2, 3, 4], 'primary key 2': [1, 2, 3, 4]},
                                       index=[1, 2, 3, 4])

        self.assertEqual(find_primary_key(df_primary_key), 'primary key')
        self.assertEqual(find_primary_key(df_not_primary), None)
        self.assertEqual(find_primary_key(df_primary_key_sec), 'primary key')
        self.assertEqual(find_primary_key(df_both_primary), 'primary key 1')

    # to change format of the column header of data frame.
    def test_reformat_col_headers(self):

        df1_reformat_header = pd.DataFrame({'Primary name': [1, 2, 3, 4, 5]})
        df1_reformat_check = pd.DataFrame({'primary_name': [1, 2, 3, 4, 5]})
        df2_reformat_header = pd.DataFrame({'Column header 1': [1, 2], 'Columnhead2': [2, 3]}, index=[0, 1])
        df2_reformat_check = pd.DataFrame({'column_header_1': [1, 2], 'columnhead2': [2, 3]}, index=[0, 1])

        pd.testing.assert_frame_equal(reformat_col_headers(df1_reformat_header), df1_reformat_check)
        pd.testing.assert_frame_equal((reformat_col_headers(df2_reformat_header)), df2_reformat_check)

    # to extract the name of the file without the path and its extension.
    def test_name_csv(self):

        file_in_csv = '/sdd/cov.csv'
        file_csv = 'C:/Users/shubh/OneDrive/Desktop/project/v_sales.csv'

        self.assertEqual(name_csv(file_in_csv), 'cov')
        self.assertEqual(name_csv(file_csv), 'v_sales')
        self.assertEqual(name_csv('ab/cd/ef/ABCD.csv'), 'ABCD')

    def tearDown(self):
        print('completed one testing')


if __name__ == "__main__":
    unittest.main()
